package in.blueignis.storm.util.uclassify;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

public class UClassifyREST {

	public static String UCLASSIFY_REST_URL = "http://uclassify.com/browse/";
	public static String ULCASSIFY_READ_API = "UiysiqXw41dtu6Piai2EyVjRsE";
	public static String ULCASSIFY_WRITE_API = "ngUJogcUtLv9AB6wre7BFAWthc";

	/**
	 * Returns the JSON Response from the website.
	 * 
	 * @param text
	 *            Text that is to be classified.
	 * @param username
	 *            Username who owns the Classifier
	 * @param classifier
	 *            Name of the classifiere
	 * @return JSON Response of the request.
	 * @throws UnsupportedEncodingException
	 */
	public String classify(String text, String username, String classifier)
			throws UnsupportedEncodingException {
		String buildingUrl = UCLASSIFY_REST_URL
				+ URLEncoder.encode(username, "UTF-8") + "/"
				+ URLEncoder.encode(classifier, "UTF-8") + "/ClassifyText?";

		buildingUrl = buildingUrl + "readKey="
				+ URLEncoder.encode(UClassifyREST.ULCASSIFY_READ_API, "UTF-8")
				+ "&text=" + URLEncoder.encode(text, "UTF-8")
				+ "&output=json&version=1.0";

		String response = doRest(buildingUrl);
		return response;
	}

	/**
	 * Call the REST based URL of uClassify
	 * 
	 * @param url
	 *            Final url with all the parameters and arguments
	 * @return Response from the Server
	 */
	private String doRest(String url) {
		try {
			URL uclassifyRest = new URL(url);
			BufferedReader responseReader = new BufferedReader(
					new InputStreamReader(uclassifyRest.openStream()));
			StringBuffer response = new StringBuffer();
			String line;
			while ((line = responseReader.readLine()) != null) {
				response.append(line);
			}
			responseReader.close();
			return response.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
