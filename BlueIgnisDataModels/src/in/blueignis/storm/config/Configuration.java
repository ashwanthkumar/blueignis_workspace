package in.blueignis.storm.config;

import java.util.Date;

/**
 * Contains the configuration parameters for the System.
 * 
 * @author Ashwanth Kumar
 * 
 */
public class Configuration {
	public final static String DATABASE_NAME = "fohubcom_blueignis";
	public final static String DATABASE_PASSWORD = "root";
	public final static String DATABASE_USER = "root";
	public final static String DATABASE_HOST = "localhost";

	public final static String JDBC_CONNECTION_STRING = "jdbc:mysql://"
			+ Configuration.DATABASE_HOST + ":3306/"
			+ Configuration.DATABASE_NAME + "?user="
			+ Configuration.DATABASE_USER + "&password="
			+ Configuration.DATABASE_PASSWORD;

	public final static String TWITTER_USERNAME = "blueignis";
	public final static String TWITTER_PASSWORD = "Live4Others";

	/**
	 * Takes in a Date object and returns a string of the form TIMESTAMP for
	 * MySQL.
	 * 
	 * @param parsableDate
	 * @return MySQL Formatted (TIMESTAMP) String
	 */
	@SuppressWarnings("deprecation")
	public static String getMySQLFormattedDate(Date parsableDate) {
		int year = parsableDate.getYear();
		int month = parsableDate.getMonth();
		int date = parsableDate.getDate();

		int hours = parsableDate.getHours();
		int minutes = parsableDate.getMinutes();
		int seconds = parsableDate.getSeconds();

		// Y-m-d H:i:s format
		return (year + "-" + month + "-" + date + " " + hours + ":" + minutes
				+ ":" + seconds);
	}
}
