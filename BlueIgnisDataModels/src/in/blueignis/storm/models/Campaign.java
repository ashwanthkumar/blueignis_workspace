package in.blueignis.storm.models;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Represents the Campaigns created by the users in the system.
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * 
 */
@DatabaseTable(tableName = "bi_campaign")
public class Campaign {
	@DatabaseField(generatedId = true)
	private Long Id;
	/**
	 * Name of the Campaign as given by the user
	 */
	@DatabaseField
	private String name;
	/**
	 * Time when the Campaign was created
	 */
	@DatabaseField
	private Date createdAt;
	/**
	 * Denotes whether the Topology for this campaign is running or not
	 */
	@DatabaseField
	private boolean isRunning;
	/**
	 * Holds which user created this campaign
	 */
	@DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
	private User user;

	public Campaign() {
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
