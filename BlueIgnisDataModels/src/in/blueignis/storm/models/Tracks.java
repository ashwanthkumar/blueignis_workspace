package in.blueignis.storm.models;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Model representing the Tracks (keywords / phrases) for each Campaign. We
 * effectively search for the tracks across Social Networks and get the data.
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * 
 */
@DatabaseTable(tableName = "bi_tracks")
public class Tracks {
	@DatabaseField(generatedId = true)
	private Long id;
	@DatabaseField
	private String name;
	@DatabaseField(columnName = "created_at")
	private Date createdAt;
	@DatabaseField(columnName = "is_archived")
	private Boolean isArchived;
	@DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
	private Campaign campaign;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getIsArchived() {
		return isArchived;
	}

	public void setIsArchived(Boolean isArchived) {
		this.isArchived = isArchived;
	}

	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}
}
