package in.blueignis.storm.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Represents the mood status of the Tweet using the uClassify Sentiment
 * Classifier.
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * 
 */
@DatabaseTable(tableName = "bi_tweetmood")
public class TweetMood {
	@DatabaseField(generatedId = true)
	private Long Id;
	@DatabaseField(index = true)
	private String mood;
	@DatabaseField
	private Float score;
	@DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true, index = true)
	private TwitterFirehouse firehouse;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getMood() {
		return mood;
	}

	public void setMood(String mood) {
		this.mood = mood;
	}

	public Float getScore() {
		return score;
	}

	public void setScore(Float score) {
		this.score = score;
	}

	public TwitterFirehouse getFirehouse() {
		return firehouse;
	}

	public void setFirehouse(TwitterFirehouse firehouse) {
		this.firehouse = firehouse;
	}
}
