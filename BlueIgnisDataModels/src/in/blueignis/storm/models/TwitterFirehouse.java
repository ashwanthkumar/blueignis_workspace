package in.blueignis.storm.models;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Represents the local twitter firehouse of the application.
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * 
 */
@DatabaseTable(tableName = "bi_firehouse")
public class TwitterFirehouse {
	@DatabaseField(generatedId = true)
	public Long id;
	@DatabaseField(index = true)
	public String text;
	@DatabaseField
	public Date createdAt;
	@DatabaseField
	public Long twitterId;
	@DatabaseField
	public Long reTweetedCount;
	@DatabaseField(index = true)
	public String source;
	@DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true)
	public TwitterUser user;

	public TwitterFirehouse() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Long getTwitterId() {
		return twitterId;
	}

	public void setTwitterId(Long twitterId) {
		this.twitterId = twitterId;
	}

	public Long getReTweetedCount() {
		return reTweetedCount;
	}

	public void setReTweetedCount(Long reTweetedCount) {
		this.reTweetedCount = reTweetedCount;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public TwitterUser getUser() {
		return user;
	}

	public void setUser(TwitterUser user) {
		this.user = user;
	}
}
