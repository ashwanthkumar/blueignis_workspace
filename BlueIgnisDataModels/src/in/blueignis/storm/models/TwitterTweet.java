package in.blueignis.storm.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Represents the mapping between the Campaign and Tweets in the firehouse. This
 * model is updated by the Campaign Topology which we will be running the
 * background.
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * 
 */
@DatabaseTable(tableName = "bi_tweets")
public class TwitterTweet {
	@DatabaseField(generatedId = true)
	private Long Id;
	/**
	 * Refers to the firehouse tweets
	 */
	@DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
	private TwitterFirehouse firehouse;
	/**
	 * Refers to the Campaign for which this tweet best matches
	 */
	@DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
	private Campaign campaign;

	public TwitterTweet() {
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public TwitterFirehouse getFirehouse() {
		return firehouse;
	}

	public void setFirehouse(TwitterFirehouse firehouse) {
		this.firehouse = firehouse;
	}

	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

}
