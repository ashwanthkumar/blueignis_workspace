package in.blueignis.storm.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Represents the User from the Twitter. This helps in per-user statistics and
 * analysis. We are searching and storing only Public Tweets, and hopefully no
 * copyright / privacy issues are formed.
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * 
 */
@DatabaseTable(tableName = "bi_tusers")
public class TwitterUser {

	@DatabaseField(generatedId = true)
	private Integer id;
	@DatabaseField(index = true, indexName = "TwitterUsersIndex")
	private Long twitterId;
	@DatabaseField
	private String name;
	@DatabaseField
	private String description;
	@DatabaseField
	private String url;
	@DatabaseField
	private String screenName;
	@DatabaseField
	private Integer followersCount;
	@DatabaseField
	private Integer friendsCount;
	@DatabaseField
	private String profileImage;

	public TwitterUser() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getTwitterId() {
		return twitterId;
	}

	public void setTwitterId(Long twitterId) {
		this.twitterId = twitterId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public Integer getFollowersCount() {
		return followersCount;
	}

	public void setFollowersCount(Integer followersCount) {
		this.followersCount = followersCount;
	}

	public Integer getFriendsCount() {
		return friendsCount;
	}

	public void setFriendsCount(Integer friendsCount) {
		this.friendsCount = friendsCount;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}
}
