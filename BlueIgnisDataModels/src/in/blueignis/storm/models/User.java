package in.blueignis.storm.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Represents the general users of the system.
 * 
 * @author Ashwanth Kumar
 * 
 */
@DatabaseTable(tableName = "bi_users")
public class User {
	@DatabaseField(generatedId = true)
	private Long Id;
	@DatabaseField
	private String name;
	@DatabaseField
	private String email;
	@DatabaseField
	private String password;
	@DatabaseField
	private String company;

	public User() {
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
}
