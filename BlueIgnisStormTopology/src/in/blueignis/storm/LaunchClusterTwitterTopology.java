package in.blueignis.storm;

import in.blueignis.storm.bolt.SentimentAnalyseBolt;
import in.blueignis.storm.bolt.TweetExtractor;
import in.blueignis.storm.config.Configuration;
import in.blueignis.storm.spout.TwitterStreamingSpout;

import java.sql.SQLException;

import backtype.storm.Config;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.TopologyBuilder;

/**
 * Launches the Cluster wide Twitter Topology. This starts the topology which
 * takes care of all the Twitter related campaigns.
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * @author Arulmani Kuppusamy
 * 
 */
public class LaunchClusterTwitterTopology {
	public static void main(String args[]) throws InterruptedException,
			SQLException, AlreadyAliveException, InvalidTopologyException {

		TopologyBuilder builder = new TopologyBuilder();
		builder.setSpout("tweeter",
				new TwitterStreamingSpout(Configuration.TWITTER_USERNAME,
						Configuration.TWITTER_PASSWORD), 1);
		builder.setBolt("tweetDumper", new TweetExtractor(), 10)
				.shuffleGrouping("tweeter");
		builder.setBolt("sentimentAnalyser", new SentimentAnalyseBolt(), 5)
				.shuffleGrouping("tweetDumper");

		Config config = new Config();
		config.setDebug(true);
		config.setNumWorkers(5);

		StormSubmitter.submitTopology("ClusterTwitterTopolgy", config,
				builder.createTopology());
	}
}
