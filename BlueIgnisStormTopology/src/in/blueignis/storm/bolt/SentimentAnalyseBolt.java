package in.blueignis.storm.bolt;

import in.blueignis.storm.config.Configuration;
import in.blueignis.storm.models.TweetMood;
import in.blueignis.storm.models.TwitterFirehouse;
import in.blueignis.storm.util.uclassify.UClassifyREST;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.spring.DaoFactory;
import com.j256.ormlite.support.ConnectionSource;

/**
 * Storm Bolt that classifies each of the Tweet from the Local firehouse using
 * uClassify Sentiment API
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * 
 */
public class SentimentAnalyseBolt extends BaseBasicBolt {

	private static final long serialVersionUID = -2778543086904256241L;
	private static final Logger _logger = Logger
			.getLogger(SentimentAnalyseBolt.class);

	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
		try {
			ConnectionSource connectionSource = new JdbcPooledConnectionSource(
					Configuration.JDBC_CONNECTION_STRING);

			Long tweetId = input.getLong(0);

			Dao<TwitterFirehouse, Integer> TFirehouseDAO = DaoFactory
					.createDao(connectionSource, TwitterFirehouse.class);

			TwitterFirehouse firehouse = new TwitterFirehouse();
			firehouse.setId(tweetId);
			TFirehouseDAO.refresh(firehouse);

			UClassifyREST restAPI = new UClassifyREST();
			String classifyResponse = restAPI.classify(firehouse.getText(),
					"uClassify", "Sentiment");
			_logger.info(classifyResponse);

			if (classifyResponse != null) {
				JSONParser parser = new JSONParser();
				JSONObject _firehouse = (JSONObject) parser
						.parse(classifyResponse);
				JSONObject sentimentClass = (JSONObject) _firehouse.get("cls1");

				Float nScore = Float.parseFloat(sentimentClass.get("negative")
						.toString());
				Float pScore = Float.parseFloat(sentimentClass.get("positive")
						.toString());

				TweetMood mood = new TweetMood();
				mood.setFirehouse(firehouse);
				if ((nScore >= 0.4 && nScore <= 0.6)
						|| (pScore >= 0.4 && pScore <= 0.6)) {
					mood.setMood("neutral");
					mood.setScore((pScore + nScore) / 2);
				} else if (nScore > pScore) {
					mood.setMood("negative");
					mood.setScore(nScore);
				} else {
					mood.setMood("positive");
					mood.setScore(pScore);
				}
				Dao<TweetMood, Integer> TMoodDAO = DaoFactory.createDao(
						connectionSource, TweetMood.class);
				TMoodDAO.createOrUpdate(mood);

			} else {
				// TODO: If the response is null chuck that we will need to
				// handle it later then
				_logger.warn("TweetID - "
						+ firehouse.getTwitterId()
						+ " has not been classified as positive / negative. Unknown server error.");
			}

			collector.emit(new Values(firehouse.getId()));
			connectionSource.close();
		} catch (Exception e) {
			_logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("tweet_id"));
	}
}
