package in.blueignis.storm.bolt;

import in.blueignis.storm.config.Configuration;
import in.blueignis.storm.models.TwitterFirehouse;
import in.blueignis.storm.models.TwitterUser;

import java.text.SimpleDateFormat;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import backtype.storm.Config;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.spring.DaoFactory;
import com.j256.ormlite.support.ConnectionSource;

/**
 * Storm Bolt, that extracts the JSON serialized status messages and put them on
 * local firehouse.
 * 
 * It emits the PK of the Firehouse.
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * 
 */
public class TweetExtractor extends BaseBasicBolt {

	private static final long serialVersionUID = -8412454089571998081L;

	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
		try {
			ConnectionSource connectionSource = new JdbcPooledConnectionSource(
					Configuration.JDBC_CONNECTION_STRING);

			String statusJSON = input.getValueByField("tweet").toString();
			JSONParser parser = new JSONParser();
			JSONObject _firehouse = (JSONObject) parser.parse(statusJSON);
			JSONObject _tuser = (JSONObject) _firehouse.get("user");

			Dao<TwitterUser, Integer> TUserDAO = DaoFactory.createDao(
					connectionSource, TwitterUser.class);

			TwitterUser tuser = new TwitterUser();
			tuser.setDescription(((_tuser.get("Description") != null) ? _tuser
					.get("Description").toString() : null));
			tuser.setFollowersCount(Integer.parseInt(_tuser.get(
					"FollowersCount").toString()));
			tuser.setFriendsCount(Integer.parseInt(_tuser.get("FriendsCount")
					.toString()));
			tuser.setName(_tuser.get("Name").toString());
			tuser.setProfileImage(_tuser.get("ProfileImage").toString());
			tuser.setScreenName(_tuser.get("ScreenName").toString());
			tuser.setTwitterId(Long.parseLong(_tuser.get("TwitterId")
					.toString()));
			tuser.setUrl(((_tuser.get("URL") != null) ? _tuser.get("URL")
					.toString() : null));

			TUserDAO.createOrUpdate(tuser);

			Dao<TwitterFirehouse, Integer> TFirehouseDAO = DaoFactory
					.createDao(connectionSource, TwitterFirehouse.class);

			TwitterFirehouse firehouse = new TwitterFirehouse();
			firehouse.setCreatedAt(SimpleDateFormat.getInstance().parse(
					_firehouse.get("createdAt").toString()));
			firehouse.setReTweetedCount(Long.parseLong(_firehouse.get(
					"ReTweetedCount").toString()));
			firehouse.setSource(_firehouse.get("source").toString());
			firehouse.setText(_firehouse.get("text").toString());
			firehouse.setTwitterId(Long.parseLong(_firehouse.get("twitterId")
					.toString()));
			firehouse.setUser(tuser);

			TFirehouseDAO.createOrUpdate(firehouse);

			// Need to emit something here?!
			collector.emit(new Values(firehouse.getId()));

			connectionSource.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("tweet"));
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		Config config = new Config();
		config.setMaxTaskParallelism(50);
		return config;
	}
}
