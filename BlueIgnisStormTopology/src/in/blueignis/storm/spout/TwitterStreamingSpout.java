package in.blueignis.storm.spout;

import in.blueignis.storm.config.Configuration;
import in.blueignis.storm.models.Tracks;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import twitter4j.FilterQuery;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;
import backtype.storm.Config;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichSpout;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.spring.DaoFactory;
import com.j256.ormlite.support.ConnectionSource;

/**
 * Spout that plugs to Twitter Streaming API and stream in the tweets to the
 * system. It also checks periodically for any new Tracks from the Redis.
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * 
 */
public class TwitterStreamingSpout implements IRichSpout {
	private static final long serialVersionUID = 2679682778881735742L;
	private static Logger _logger = Logger
			.getLogger(TwitterStreamingSpout.class);

	private String twitterUsername;
	private String twitterPassword;
	private LinkedBlockingQueue<Status> stream;
	private TwitterStream _stream;
	private SpoutOutputCollector _collector;

	private FilterQuery filter;

	private static Long currentTime = 0L;
	private ConnectionSource connectionSource;

	/**
	 * Create a new Twitter Streaming Spout with the given Twitter Credentials.
	 * 
	 * @param username
	 * @param password
	 */
	public TwitterStreamingSpout(String username, String password) {
		this.twitterUsername = username;
		this.twitterPassword = password;
	}

	@Override
	public void open(@SuppressWarnings("rawtypes") Map conf,
			TopologyContext context, SpoutOutputCollector collector) {
		stream = new LinkedBlockingQueue<Status>(5000);
		_collector = collector;
		StatusListener tweetListener = new StatusListener() {

			@Override
			public void onException(Exception arg0) {
				/**
				 * FAIL Silently
				 */
			}

			@Override
			public void onTrackLimitationNotice(int arg0) {
				/**
				 * We might want to implement this in the near future.
				 */
			}

			@Override
			public void onStatus(Status arg0) {
				storeRawJSON(arg0);
			}

			@Override
			public void onScrubGeo(long arg0, long arg1) {
				/**
				 * We are currently not storing GEO notices so it can be safely
				 * ignored
				 **/
			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice arg0) {
				/**
				 * TODO: Yet to implement. Sometimes the deletion notice will
				 * come out of order so we might also need to hold some queue to
				 * implement the Deletion delegation notice.
				 */
			}
		};

		try {
			connectionSource = new JdbcPooledConnectionSource(
					Configuration.JDBC_CONNECTION_STRING);
			/**
			 * Create a TwitterStream Instace using Factory settings +
			 * credentials.
			 */
			TwitterStreamFactory fact = new TwitterStreamFactory(
					new ConfigurationBuilder().setUser(twitterUsername)
							.setPassword(twitterPassword).build());
			_stream = fact.getInstance();
			_stream.addListener(tweetListener);

			updateFilter();
			/**
			 * Set the FilterQuery for the TwitterStream. This creates a new
			 * thread and streams the tweets.
			 */
			_stream.filter(filter);
		} catch (Exception e) {
			_logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	private void storeRawJSON(Status raw) {

		boolean insertStatus = stream.offer(raw);
		while (!insertStatus) { // Keep trying every 100ms to add the
								// status to the queue in-case the queue
								// in full
			Utils.sleep(100);
			insertStatus = stream.offer(raw);
		}
	}

	@Override
	public void close() {
		_stream.shutdown();
	}

	/**
	 * Update the filter with the latest tracks from the Datastore.
	 */
	protected void updateFilter() {
		try {
			Dao<Tracks, Long> TracksDAO = DaoFactory.createDao(
					connectionSource, Tracks.class);
			List<Tracks> allTracks = TracksDAO.queryForAll();
			ArrayList<String> tracks = new ArrayList<String>();

			for (Tracks track : allTracks) {
				tracks.add(track.getName());
			}

			/**
			 * Create a FilterQueury for use with Twitter Streaming
			 */
			filter = new FilterQuery();
			String trackList[] = new String[tracks.size()];
			tracks.toArray(trackList);
			filter.track(trackList);
		} catch (SQLException sqlException) {
			_logger.error(sqlException.getMessage());
			sqlException.printStackTrace();
		}
	}

	@Override
	public void activate() {
	}

	@Override
	public void deactivate() {
	}

	@Override
	public void nextTuple() {
		/**
		 * Try to execute this for every 60 milliseconds, as there is a very
		 * high chance that Storm will call nextTuple on the Spout on < 60
		 * seconds
		 */
		if (currentTime == 0) {
			currentTime = Calendar.getInstance().getTimeInMillis();
		} else {
			long realTime = Calendar.getInstance().getTimeInMillis();
			if ((realTime - currentTime) > (1000L * 60L)) {
				currentTime = Calendar.getInstance().getTimeInMillis();
				// Time to check for new tracks.
				updateFilter();
				/**
				 * Set the FilterQuery for the TwitterStream. This creates a new
				 * thread and streams the tweets.
				 */
				_stream.filter(filter);
			}
		}

		Status status = stream.poll();
		if (status == null)
			Utils.sleep(100);
		else {
			User user = status.getUser();
			Map<String, Object> tuser = new HashMap<String, Object>();
			tuser.put("Description", (user.getDescription()));
			tuser.put("FollowersCount", (user.getFollowersCount()));
			tuser.put("FriendsCount", (user.getFriendsCount()));
			tuser.put("Name", (user.getName()));
			tuser.put("ProfileImage", (user.getProfileImageURL().toString()));
			tuser.put("ScreenName", (user.getScreenName()));
			tuser.put("TwitterId", (user.getId()));
			tuser.put("Url", ((user.getURL() != null) ? user.getURL()
					.toString() : null));

			Map<String, Object> firehouse = new HashMap<String, Object>();
			firehouse.put("createdAt",
					SimpleDateFormat.getInstance()
							.format(status.getCreatedAt()));
			firehouse.put("ReTweetedCount", status.getRetweetCount());
			firehouse.put("source", status.getSource());

			firehouse.put("text", status.getText());
			firehouse.put("twitterId", status.getId());
			firehouse.put("user", tuser);

			String json = JSONObject.toJSONString(firehouse);
			_logger.info(json);
			_collector.emit(new Values(json));
		}
	}

	@Override
	public void ack(Object msgId) {
		/**
		 * ACK silently
		 */
	}

	@Override
	public void fail(Object msgId) {
		/**
		 * FAIL silently
		 */
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("tweet"));
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		Config config = new Config();
		/**
		 * To make sure we dont create un-necessary connections to the Twitter
		 * Streaming API.
		 */
		config.setMaxTaskParallelism(1);
		return config;
	}

}
