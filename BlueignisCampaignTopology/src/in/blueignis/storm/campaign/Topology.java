package in.blueignis.storm.campaign;

import in.blueignis.storm.campaign.bolt.SentimentDashboardPusher;
import in.blueignis.storm.campaign.bolt.TweetAssigner;
import in.blueignis.storm.campaign.bolt.TweetDashboardPusher;
import in.blueignis.storm.campaign.spout.TweetSearchSpout;

import org.apache.log4j.Logger;

import backtype.storm.Config;
import backtype.storm.StormSubmitter;
import backtype.storm.topology.TopologyBuilder;

/**
 * Topology that submits the Campaign Topology to the Storm Cluster.
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * 
 */
public class Topology {
	private static final Logger _logger = Logger.getLogger(Topology.class);

	/**
	 * Main Application that configures the topology based on the Camapaign Id
	 * passed during the startup of the program and submits it to the Storm
	 * Cluster.
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// Get the required Campaign Id
		Long campaignId = Long.parseLong(args[0]);

		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout("TweetSearchSpout", new TweetSearchSpout(campaignId));
		builder.setBolt("TweetAssigner", new TweetAssigner(campaignId))
				.shuffleGrouping("TweetSearchSpout");
		builder.setBolt("TweetDashboardPusher",
				new TweetDashboardPusher(campaignId)).shuffleGrouping(
				"TweetAssigner");
		builder.setBolt("SentimentDashboardPusher",
				new SentimentDashboardPusher(campaignId)).shuffleGrouping(
				"TweetAssigner");

		// Defining the Configuration parameters
		Config config = new Config();
		config.setDebug(true);
		config.setNumWorkers(30);
		config.setMaxSpoutPending(5000);

		// Submit the Topology
		StormSubmitter.submitTopology(campaignId + "_CampaignTopology", config,
				builder.createTopology());
		_logger.info("Starting the Topology for the Campaign Id " + campaignId);
	}
}
