package in.blueignis.storm.campaign.bolt;

import in.blueignis.storm.config.Configuration;
import in.blueignis.storm.models.TweetMood;
import in.blueignis.storm.models.TwitterFirehouse;
import in.blueignis.storm.models.TwitterUser;
import in.blueignis.storm.util.pusherapp.Pusher;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;
import backtype.storm.utils.Utils;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.spring.DaoFactory;
import com.j256.ormlite.support.ConnectionSource;

/**
 * Storm Bolt that takes in the Assigned Tweet Id and push to the Campaign
 * Realtime Twitter Realtime Dashboard through Pusher.
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * 
 */
public class SentimentDashboardPusher extends BaseBasicBolt {
	private static final Logger _logger = Logger
			.getLogger(SentimentDashboardPusher.class);
	private static final long serialVersionUID = 3229471247099007040L;
	private Long campaignId;

	public SentimentDashboardPusher(Long campaignId) {
		this.campaignId = campaignId;
	}

	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
		try {
			ConnectionSource connectionSource = new JdbcPooledConnectionSource(
					Configuration.JDBC_CONNECTION_STRING);
			Dao<TwitterFirehouse, Long> TFireHouseDAO = DaoFactory.createDao(
					connectionSource, TwitterFirehouse.class);
			Dao<TwitterUser, Long> TTwitterUserDAO = DaoFactory.createDao(
					connectionSource, TwitterUser.class);
			Dao<TweetMood, Long> TTweetMood = DaoFactory.createDao(
					connectionSource, TweetMood.class);

			Long firehouseId = input.getLong(0);
			TwitterFirehouse firehouse = new TwitterFirehouse();
			firehouse.setId(firehouseId);
			TFireHouseDAO.refresh(firehouse);

			List<TweetMood> moods = TTweetMood.queryForEq("firehouse_id",
					firehouse.getId());
			_logger.info(firehouse.getId());
			_logger.info(moods.size());
			if (moods.size() > 0) {

				TweetMood mood = moods.get(0);

				TwitterUser user = new TwitterUser();
				user.setId(firehouse.getUser().getId());
				TTwitterUserDAO.refresh(user);

				Map<String, Object> jsonObject = new HashMap<String, Object>();
				jsonObject.put("tweet", firehouse.getText());
				jsonObject.put("tweet_id", firehouse.getTwitterId());
				jsonObject.put("user_img", user.getProfileImage());
				jsonObject.put("user_name", user.getScreenName());

				Map<String, Object> _message = new HashMap<String, Object>();
				_message.put("message", jsonObject);
				String message = JSONObject.toJSONString(_message);

				if (mood.getMood().equals("positive")) {
					_logger.info("Pushing Positive Tweet");
					Pusher.triggerPush(
							"blueignis-sentiment-positive-dashboard-"
									+ campaignId, "new_tweet", message);
				} else if (mood.getMood().equals("negative")) {
					_logger.info("Pushing Negative Tweet");
					Pusher.triggerPush(
							"blueignis-sentiment-negative-dashboard-"
									+ campaignId, "new_tweet", message);
				}
			} else {
				Utils.sleep(100);
			}
			connectionSource.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
	}
}
