package in.blueignis.storm.campaign.bolt;

import in.blueignis.storm.config.Configuration;
import in.blueignis.storm.models.Campaign;
import in.blueignis.storm.models.TwitterFirehouse;
import in.blueignis.storm.models.TwitterTweet;

import org.apache.log4j.Logger;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.spring.DaoFactory;
import com.j256.ormlite.support.ConnectionSource;

/**
 * Storm Bolt to assign the Firehouse Tweets to the Campaign and hold the
 * reference.
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * 
 */
public class TweetAssigner extends BaseBasicBolt {
	private static final long serialVersionUID = 4616719993517537117L;
	private ConnectionSource connectionSource;
	private static final Logger _logger = Logger.getLogger(TweetAssigner.class);
	private Long campaignId;

	public TweetAssigner(Long cId) {
		campaignId = cId;
	}

	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
		Long tweetFirehouseId = input.getLong(0);
		try {
			connectionSource = new JdbcPooledConnectionSource(
					Configuration.JDBC_CONNECTION_STRING);
			Dao<TwitterFirehouse, Long> TFireHouseDAO = DaoFactory.createDao(
					connectionSource, TwitterFirehouse.class);
			Dao<Campaign, Long> TCampaignDAO = DaoFactory.createDao(
					connectionSource, Campaign.class);
			Dao<TwitterTweet, Long> TTwitterTweetDAO = DaoFactory.createDao(
					connectionSource, TwitterTweet.class);

			TwitterFirehouse tweet = new TwitterFirehouse();
			tweet.setId(tweetFirehouseId);
			TFireHouseDAO.refresh(tweet);

			Campaign campaign = new Campaign();
			campaign.setId(campaignId);
			TCampaignDAO.refresh(campaign);

			TwitterTweet newTweet = new TwitterTweet();
			// Link the Firehouse reference
			newTweet.setFirehouse(tweet);
			// Link the Campaign reference
			newTweet.setCampaign(campaign);
			TTwitterTweetDAO.createOrUpdate(newTweet);

			// Again send the Long Id
			collector.emit(new Values(tweet.getId()));
			connectionSource.close();
		} catch (Exception e) {
			_logger.error(e.getMessage());
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("tweetid"));
	}

}
