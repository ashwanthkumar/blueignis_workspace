package in.blueignis.storm.campaign.bolt;

import in.blueignis.storm.config.Configuration;
import in.blueignis.storm.models.TwitterFirehouse;
import in.blueignis.storm.models.TwitterUser;
import in.blueignis.storm.util.pusherapp.Pusher;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.spring.DaoFactory;
import com.j256.ormlite.support.ConnectionSource;

/**
 * Storm Bolt that takes in the Assigned Tweet Id and push to the Campaign
 * Realtime Twitter Dashboard through Pusher.
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * 
 */
public class TweetDashboardPusher extends BaseBasicBolt {

	private static final long serialVersionUID = 3229471247099007040L;
	private Long campaignId;

	public TweetDashboardPusher(Long campaignId) {
		this.campaignId = campaignId;
	}

	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
		try {
			ConnectionSource connectionSource = new JdbcPooledConnectionSource(
					Configuration.JDBC_CONNECTION_STRING);
			Dao<TwitterFirehouse, Long> TFireHouseDAO = DaoFactory.createDao(
					connectionSource, TwitterFirehouse.class);
			Dao<TwitterUser, Long> TTwitterUserDAO = DaoFactory.createDao(
					connectionSource, TwitterUser.class);

			Long firehouseId = input.getLong(0);
			TwitterFirehouse firehouse = new TwitterFirehouse();
			firehouse.setId(firehouseId);
			TFireHouseDAO.refresh(firehouse);

			TwitterUser user = new TwitterUser();
			user.setId(firehouse.getUser().getId());
			TTwitterUserDAO.refresh(user);

			Map<String, Object> jsonObject = new HashMap<String, Object>();
			jsonObject.put("tweet", firehouse.getText());
			jsonObject.put("tweet_id", firehouse.getTwitterId());
			jsonObject.put("user_img", user.getProfileImage());
			jsonObject.put("user_name", user.getScreenName());

			Map<String, Object> _message = new HashMap<String, Object>();
			_message.put("message", jsonObject);
			String message = JSONObject.toJSONString(_message);

			Pusher.triggerPush("blueignis-twitter-dashboard-" + campaignId,
					"new_tweet", message);
			connectionSource.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
	}
}
