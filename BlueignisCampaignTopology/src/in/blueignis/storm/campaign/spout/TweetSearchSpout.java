package in.blueignis.storm.campaign.spout;

import in.blueignis.storm.config.Configuration;
import in.blueignis.storm.models.Tracks;
import in.blueignis.storm.models.TwitterFirehouse;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import backtype.storm.Config;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichSpout;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.spring.DaoFactory;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;

/**
 * Storm Spout which searches for tweets for the current campaign and emits them
 * in the topology.
 * 
 * @author Ashwanth Kumar / ashwanthkumar.in
 * 
 */
public class TweetSearchSpout implements IRichSpout {
	private static final long serialVersionUID = 3861920118270192791L;
	private static final Logger _logger = Logger
			.getLogger(TweetSearchSpout.class);

	private LinkedBlockingQueue<TwitterFirehouse> firehouseQueue;
	private Long lastIndex;

	private ConnectionSource connectionSource;
	private Long campaignId;

	private SpoutOutputCollector _collector;

	public TweetSearchSpout(Long campaignId) {
		this.campaignId = campaignId;
	}

	@Override
	public void open(@SuppressWarnings("rawtypes") Map conf,
			TopologyContext context, SpoutOutputCollector collector) {
		try {
			connectionSource = new JdbcPooledConnectionSource(
					Configuration.JDBC_CONNECTION_STRING);
			firehouseQueue = new LinkedBlockingQueue<TwitterFirehouse>(600);
			lastIndex = 0L;
			_collector = collector;
		} catch (SQLException e) {
			_logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Re-fill the queue with tweets that needs to be processed
	 * 
	 * @param lastIndex
	 *            Last Tweet that was properly processed
	 */
	private void getMoreTweets() {
		try {

			Dao<Tracks, Long> TracksDAO = DaoFactory.createDao(
					connectionSource, Tracks.class);
			List<Tracks> tracks = TracksDAO.queryBuilder()
					.selectColumns("id", "name").distinct().where()
					.eq("campaign_id", campaignId).and()
					.eq("is_archived", false).query();

			Dao<TwitterFirehouse, Long> TFireHouseDAO = DaoFactory.createDao(
					connectionSource, TwitterFirehouse.class);

			Where<TwitterFirehouse, Long> trackSearch = TFireHouseDAO
					.queryBuilder().limit(500L).orderBy("id", true).where();

			for (Tracks track : tracks) {
				trackSearch = trackSearch.like("text", "%" + track.getName()
						+ "%");
			}
			if (tracks.size() > 0) {
				// Add OR clause to all the tracks that were added
				trackSearch = trackSearch.or(tracks.size());
				// make sure we try to get all the tweets possible
				trackSearch = trackSearch.and().gt("id", lastIndex).and();
				trackSearch = trackSearch.notIn("id",
						"(select firehouse_id from bi_tweets)");
				List<TwitterFirehouse> requiredTweets = trackSearch.query();
				for (TwitterFirehouse tweet : requiredTweets) {
					boolean status = firehouseQueue.offer(tweet);
					if (!status) {
						lastIndex = tweet.getId();
						break;
					}
					// Make sure we always have the latest version with us
					lastIndex = tweet.getId();
				}
			}
		} catch (SQLException e) {
			_logger.error(e.getMessage());
			e.printStackTrace();
		}

	}

	@Override
	public void close() {
		try {
			connectionSource.close();
		} catch (SQLException e) {
			_logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void activate() {
	}

	@Override
	public void deactivate() {
	}

	@Override
	public void nextTuple() {
		TwitterFirehouse firehouse = firehouseQueue.poll();
		if (firehouse == null) {
			// Time to re-fill our queue with next 500 tweets
			getMoreTweets();
			Utils.sleep(100);
		} else {
			_collector.emit(new Values(firehouse.getId()));
			_logger.info(firehouse.getId());
		}
	}

	@Override
	public void ack(Object msgId) {
		/**
		 * ACK Silently
		 */
	}

	@Override
	public void fail(Object msgId) {
		/**
		 * FAIL Silently
		 */
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("tweetid"));
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		Config config = new Config();
		config.setDebug(true);
		config.setMaxSpoutPending(100);
		config.setNumWorkers(2);
		return config;
	}
}
