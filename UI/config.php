<?php

require_once(__DIR__ . "/lib/class.db.php");

/**
 *	Database related configurations
 */
$db_name = "blueignis";
$db_user = "root";
$db_host = "localhost";
$db_pass = "root";

global $db;
$db = new db("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);

global $memcache;
// Connect to the local memcache server instance
$memcache = memcache_connect("localhost", 11211);

date_default_timezone_set("Asia/Kolkata");

session_start();

