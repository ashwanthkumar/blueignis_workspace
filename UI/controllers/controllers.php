<?php

/**
 *	Campaign.php
 *
 *	Defines all the Campaign related dispatch / route implementations to be used
 *	by the application. 
 *
 *	@package	BlueIgnis
 *	@component	Controllers
 */

/**
 *	Render the Campaign Specific Dashboard
 *
 *	@method	GET
 *	@route	/campaign/:id/realtime/twitter
 */
function campaignRealtimeTwitterDashboard($id) {
	if(!isset($_SESSION['userauth'])) {	// Check if the user authenticated before rendering this page
		// Login the user
		return redirect('/login');
	}

	global $db;

	$campaign = $db->select("bi_campaign", "id = :id", array(":id" => $id));
	$campaign = $campaign[0];
	
	$campaigns = $db->select("bi_campaign", "user_id = :userid", array(":userid" => $_SESSION['authId']));

	layout('dashboard.html.php');
	// Getting the user from the session
	set('user', $_SESSION['user']);
	set('isTwitterDashboard', true);
	set('is_campaign', true);
	set('currentCampaignId', $id);
	set('campaignName', $campaign['name']);
	set('campaigns', $campaigns);
	set('authId', $_SESSION['authId']);
	set('user', $_SESSION['user']);
	set('db', $db);
	set('title', "Realtime Twitter Feeds");

	return render("twitter_dashboard.html.php");
}

/**
 *	Render the Campaign Specific Sentiment Dashboard
 *
 *	@method	GET
 *	@route	/campaign/:id/realtime/sentiment
 */
function campaignRealtimeSentimentDashboard($id) {
	if(!isset($_SESSION['userauth'])) {	// Check if the user authenticated before rendering this page
		// Login the user
		return redirect('/login');
	}

	global $db;
	
	$campaign = $db->select("bi_campaign", "id = :id", array(":id" => $id));
	$campaign = $campaign[0];
	
	$campaigns = $db->select("bi_campaign", "user_id = :userid", array(":userid" => $_SESSION['authId']));

	layout('dashboard.html.php');
	// Getting the user from the session
	set('user', $_SESSION['user']);
	set('isSentimentDashboard', true);
	set('is_campaign', true);
	set('currentCampaignId', $id);
	set('campaignName', $campaign['name']);
	set('campaigns', $campaigns);
	set('authId', $_SESSION['authId']);
	set('user', $_SESSION['user']);
	set('db', $db);
	set('title', "Realtime Sentiment Feeds");

	return render("sentiment_dashboard.html.php");
}

/**
 *	Renders view for creating a new Campaign
 *
 *	@method	GET
 *	@route	/campaign/create
 */
function createNewCampaign() {
	if(!isset($_SESSION['userauth'])) {	// Check if the user authenticated before rendering this page
		// Login the user
		return redirect('/login');
	}

	global $db;	
	$campaigns = $db->select("bi_campaign", "user_id = :userid", array(":userid" => $_SESSION['authId']));


	layout('dashboard.html.php');
	// Getting the user from the session
	set('user', $_SESSION['user']);
	set('currentCampaignId', -1);	// Since there is no campaign
	set('campaignName', "Create New Campaign");
	set('createCampaign', true); 
	set('campaigns', $campaigns);
	set('authId', $_SESSION['authId']);
	set('user', $_SESSION['user']);
	set('db', $db);
	
	return render("new_campaign.html.php");
}

/**
 *	Renders the Campaign dashboard
 *
 *	@method	GET
 *	@route	/campaign/:id
 */
function campaignDashboard($id) {
	if(!isset($_SESSION['userauth'])) {	// Check if the user authenticated before rendering this page
		// Login the user
		return redirect('/login');
	}

	global $db;	
	$campaign = $db->select("bi_campaign", "id = :id", array(":id" => $id));
	$campaign = $campaign[0];
	
	$campaigns = $db->select("bi_campaign", "user_id = :userid", array(":userid" => $_SESSION['authId']));


	layout('dashboard.html.php');
	// Getting the user from the session
	set('user', $_SESSION['user']);
	set('is_campaign', true);
	set('currentCampaignId', $id);
	set('campaignName', $campaign['name']);
	set('campaigns', $campaigns);
	set('authId', $_SESSION['authId']);
	set('user', $_SESSION['user']);
	set('db', $db);
	set('title', "Dashboard");
	
	return render("campaign_dashboard.html.php");
}

/**
 *	Render Key Influencers Dashboard page 
 *
 *	@method	GET
 *	@route	/campaign/:id/report/ki
 */
function campaignReportKeyInfluencers($id) {
	if(!isset($_SESSION['userauth'])) {	// Check if the user authenticated before rendering this page
		// Login the user
		return redirect('/login');
	}

	global $db;	
	$campaign = $db->select("bi_campaign", "id = :id", array(":id" => $id));
	$campaign = $campaign[0];
	
	$campaigns = $db->select("bi_campaign", "user_id = :userid", array(":userid" => $_SESSION['authId']));


	layout('dashboard.html.php');
	// Getting the user from the session
	set('user', $_SESSION['user']);
	set('isKI', true);
	set('is_campaign', true);
	set('currentCampaignId', $id);
	set('campaignName', $campaign['name']);
	set('campaigns', $campaigns);
	set('authId', $_SESSION['authId']);
	set('user', $_SESSION['user']);
	set('db', $db);
	set('title', "Key Influencers");

	return render("keyinfluencers.html.php");
}

/**
 *	Render Market Reaction Dashboard page 
 *
 *	@method	GET
 *	@route	/campaign/:id/report/twitter/reaction
 */
function campaignReportOverallMarketReaction($id) {
	if(!isset($_SESSION['userauth'])) {	// Check if the user authenticated before rendering this page
		// Login the user
		return redirect('/login');
	}

	global $db;	
	$campaign = $db->select("bi_campaign", "id = :id", array(":id" => $id));
	$campaign = $campaign[0];
	
	$campaigns = $db->select("bi_campaign", "user_id = :userid", array(":userid" => $_SESSION['authId']));


	layout('dashboard.html.php');
	// Getting the user from the session
	set('user', $_SESSION['user']);
	set('is_campaign', true);
	set('currentCampaignId', $id);
	set('isMarketReaction', true);
	set('campaignName', $campaign['name']);
	set('campaigns', $campaigns);
	set('authId', $_SESSION['authId']);
	set('user', $_SESSION['user']);
	set('db', $db);
	set('title', "Market Reaction");

	return render("marketreaction_dashboard.html.php");
}

/**
 *	Render Market Reaction Dashboard page 
 *
 *	@method	GET
 *	@route	/campaign/:id/report/twitter/reaction
 */
function campaignReportTwitterOverallSourceDistribution($id) {
	if(!isset($_SESSION['userauth'])) {	// Check if the user authenticated before rendering this page
		// Login the user
		return redirect('/login');
	}

	global $db;	
	$campaign = $db->select("bi_campaign", "id = :id", array(":id" => $id));
	$campaign = $campaign[0];
	
	$campaigns = $db->select("bi_campaign", "user_id = :userid", array(":userid" => $_SESSION['authId']));


	layout('dashboard.html.php');
	// Getting the user from the session
	set('user', $_SESSION['user']);
	set('is_campaign', true);
	set('currentCampaignId', $id);
	set('isTwitterSource', true);
	set('campaignName', $campaign['name']);
	set('campaigns', $campaigns);
	set('authId', $_SESSION['authId']);
	set('user', $_SESSION['user']);
	set('db', $db);
	set('title', "Tweet Source Distribution");

	return render("sourcedistribution_dashboard.html.php");
}

/**
 *	Run the Campaign after creating it
 *
 *	@method	POST
 *	@route	/campaign/create/run
 */
function runCampaign() {
	if(!isset($_SESSION['userauth'])) {	// Check if the user authenticated before rendering this page
		// Login the user
		return redirect('/login');
	}

	global $db;

	$tracks = $_POST['campaign_tracks'];
	$tracks = explode(",", $tracks);
	
	print_r($tracks);

	$service = $_POST['campaign_service'];
	$name = $_POST['campaign_name'];
	
	$campaign = array(
		"name" => $name, // TODO: May be sometime we should check for possible duplicates
		"createdAt" => date("Y-m-d H:i:s"),
		"user_id" => $_SESSION['authId'],
	);
	
	$db->insert("bi_campaign", $campaign);
	$newCid = $db->lastInsertId();	// Get the Id value of the Campaign value that was inserted last
	
	foreach($tracks as $track) {
		$t = array(
			"name" => strtolower(trim($track)),	// Always rename the words to lower string. 
			"is_archived" => false,
			"campaign_id" => $newCid,
			"created_at" => date("Y-m-d H:i:s"),
		);
		
		// Add the new track to the redis for processing within the next 1 -2 minute(s)
		$db->insert("bi_tracks", $t);
	}

	// Starting the Campaign Threaded Deamon Process
	// TODO Need to submit the Campaign to the Storm
	// exec("php /home/blueignis/blueignis/spout/service/ThreadedCampaignQueueWorker.php > /home/blueignis/blueignis/spot/service/campaign-" . $newCid . ".log 2>&1 &"); 
	
	flash('created_new', true);
	flash('created_status', 'success');
	flash('created_log', 'Your Campaign has been queued and will start momentarily.');
	
	// Now redirect the user to the Campaign Dashboard
	return redirect(url_for("/campaign/$newCid"));
}


/**
 *	Delete a Campaign completely along with its related data
 *
 *	@method	GET
 *	@route	^/campaign/(\d+)/delete
 */
function deleteCampaign($id) {
	global $db;
	
	$db->run("delete from bi_tweetmood where firehouse_id IN (select id from bi_tweets where campaign_id = :cid); delete from bi_tweets where campaign_id = :cid; delete from bi_tracks where campaign_id = :cid; delete from bi_campaign where id = :cid;", array(":cid" => $id));
	
	return redirect(url_for('/campaign/create'));
}

