<?php

/**
 *	User.php
 *
 *	Defines the User related dispatch / route implementations to be used
 *	by the application. 
 *
 *	@package	BlueIgnis
 *	@component	Controllers
 */

/**
 *	Entry point of the application
 *
 *	@method	GET
 *	@route	/
 */
function blueignis() {
	if(isset($_SESSION['userauth'])) {
		return redirect('/campaign/create');
	} else {
		// Login the user
		return redirect('/login');
	}
}

/**
 *	Render the Login for the User
 *
 *	@method	GET
 *	@route	/login
 */
function login() {
	return render('login.html.php');
}

/**
 *	Render the Registration page for the User
 *
 *	@method	GET
 *	@route	/register
 */
function register() {
	if(isset($_SESSION['userauth'])) {
		return redirect('/campaign/create');
	} else {
		return render('register.html.php');
	}
}

/**
 *	Authenticate the user and redirect accordingly
 *
 *	@method	POST
 *	@route	/authenticate
 */
function authenticate() {
	global $db;
	
	$username = $_POST['username'];
	$password = md5($_POST['password']);    // all passwords are encrypted

	$user = $db->select("bi_users", "email = :username and password = :password", array(":username" => $username, ":password" => $password));
	if(count($user) > 0) {
		// Seems like user exist auth 'em
		$_SESSION['authId'] = $user[0]['Id'];
		$_SESSION['userauth'] = $_SESSION['authId'];
		
		return blueignis();
	} else {
		// Sorry invalid credentials so pass the info along
		$_SESSION['invalid_login'] = "Sorry invalid username / password";
		return login();
	}
}

/**
 *	Logout the current user
 *
 *	@method	GET
 *	@route	/logout
 */
function logout() {
	unset($_SESSION['userauth']);
	unset($_SESSION['authId']);
	
	return redirect('/login');
}

/**
 *	Proxy for serving twitter images
 *
 *	@method	GET
 *	@route	/proxy/:url
 */
function imageProxy() {
	$url = $_GET['url'];
	return render(file_get_contents($url));
}

