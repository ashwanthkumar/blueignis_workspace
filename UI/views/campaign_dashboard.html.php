<?php

content_for('body');

$tracks = $db->select("bi_tracks", "campaign_id = :cid", array(":cid" => $currentCampaignId));

$t = array();

$track_list_str = "";

foreach($tracks as $track) {
	$_t = array();
	$_t['name'] = $track['name'];
	
	$_t['is_archived'] = $track['is_archived'];
	if($_t['is_archived'] == 1) $track_list_str .= "<s>" . $_t['name'] . "</s>";
	else $track_list_str .= $_t['name'] . ", ";
	
	
	$t[] = $_t;
}


if(isset($flash['created_new']) && $flash['created_status'] == "success") {
?>
<div class="alert alert-success">	<?php echo $flash['created_log']; ?> </div>
<?php
} else if(isset($flash['created_new']) && $flash['created_status'] == "error") {
?>
<div class="alert alert-error">	<?php echo $flash['created_log']; ?> </div>
<?php
}

$campaign = $db->select("bi_campaign", "Id = :cid", array(":cid" => $currentCampaignId));

global $memcache;

$days = memcache_get($memcache, 'days_' . $currentCampaignId);
if(!$days) {
	// Since I know I want last 24 time series points, I might as well create them. 
	$days = $db->run("select HOUR(bi_firehouse.createdAt) as day , bi_firehouse.createdAt as posted_on from bi_tweets, bi_firehouse where bi_tweets.firehouse_id = bi_firehouse.id and bi_tweets.campaign_id = :cid and bi_firehouse.createdAt > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 22 HOUR) group by HOUR(bi_firehouse.createdAt) order by bi_firehouse.createdAt DESC limit 24;", array(":cid" => $currentCampaignId));
	memcache_set('days_' . $currentCampaignId, $days, 10 * 60);
}
?>
<div class="hero-unit">
	<h1>Dashboard</h1>
	<p>Tracks: <?php echo substr(trim($track_list_str), 0, -1); ?></p>
</div>

<div class="row-fluid">

	<div class="span12">
	<?php

	// Make sure we show them some graph only after we have atleast 2 time series data points
if(count($days) >= 2) {
?>
    <div id="overall_twitter_stats" style="width: 95%; height: 500px;"></div>
<?php
	} else {
?>
	<div class="alert alert-info">	We do not yet have enough data to show you any demographics. Meanwhile, you can look into the Realtime Twitter Feeds for the Campaign at Twitter Dashboard. </div>
<?php
	}
?>
    <p>
			<a class="btn" href="<?php echo url_for('/campaign/' . $currentCampaignId . '/realtime/twitter'); ?>">View Twitter Dashboard &raquo;</a>
		</p>
	</div>
</div>

<?php
end_content_for();


content_for('script');


// Make sure we show them some graph only after we have atleast 2 time series data points
if(count($days) >= 2) {
?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawSummaryChart);


  function drawSummaryChart() {
		var options = {
		  title: 'Last 24 Hour Tweet(s) Activity',
		  hAxis: {title: 'Time of Day' },
		  vAxis: {title: 'Tweet Count' },
		};
		
		var data = new google.visualization.DataTable();
		data.addColumn('datetime', 'Day Of Week');
		data.addColumn('number', 'Total Tweets');
		data.addColumn('number', 'Positive Tweets');
		data.addColumn('number', 'Negative Tweets');

		<?php
		

			/*
				CAUTION - Slows down the page loading, but gives you a better reporting of the content
				
			if(count($days) < 2) {
			//if(true) {	// Reverting to low-level reporting
				// Use low-level reporting when we have no proper dataset to work with
				$days = array();
				for($i = 23; $i >= 0; $i--) {
					$day = array();
					$day['day'] = $i;
					$day['posted_on'] = date('Y-m-d H:i:s', (round(strtotime("- $i hour") / 3600) * 3600));
				
					$days[] = $day;
				}
				
				echo "options.title = 'Current Tweet(s) Activity';";
			}
			*/

			foreach($days as $day) {
				$dailycount = memcache_get($memcache, 'days_' . $day['day'] . '_c_' . $currentCampaignId . '_total');
				if(!$dailycount) {
					$dailycount = $db->run("select count(*) as count from bi_tweets,bi_firehouse where bi_tweets.firehouse_id = bi_firehouse.id and bi_tweets.campaign_id = :cid and HOUR(bi_firehouse.createdAt) = :day  and bi_firehouse.createdAt > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 22 HOUR) ", array(":day" => $day['day'], ":cid" => $currentCampaignId));
					$dailycount = $dailycount[0];
					$dailycount = $dailycount['count'];
					
					memcache_set($memcache, 'days_' . $day['day'] . '_c_' . $currentCampaignId . '_total', $dailycount, 10 * 60);
				}

				
				$positivecount = memcache_get($memcache, 'days_' . $currentCampaignId . '_positive');
				if(!$positivecount) {
					$positivecount = $db->run("select count(*) as count from bi_tweetmood where mood = :mood and firehouse_id IN (select bi_firehouse.id from bi_tweets,bi_firehouse where bi_tweets.firehouse_id = bi_firehouse.id and HOUR(bi_firehouse.createdAt) = :day and bi_tweets.campaign_id = :cid and  bi_firehouse.createdAt > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 22 HOUR) )", array(":mood" => "positive", ":day" => $day['day'], ":cid" => $currentCampaignId));
					$positivecount = $positivecount[0];
					$positivecount = $positivecount['count'];
					
					memcache_set($memcache, 'days_' . $currentCampaignId . '_positive', $positivecount, 5 * 60);
				}
				
				
				$negativecount = memcache_get($memcache, 'days_' . $currentCampaignId . '_negative');
				if(!$negativecount) {
					$negativecount = $db->run("select count(*) as count from bi_tweetmood where mood = :mood and firehouse_id IN (select bi_firehouse.id from bi_tweets,bi_firehouse where bi_tweets.firehouse_id = bi_firehouse.id and HOUR(bi_firehouse.createdAt) = :day and bi_tweets.campaign_id = :cid and  bi_firehouse.createdAt > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 22 HOUR) )", array(":mood" => "negative", ":day" => $day['day'], ":cid" => $currentCampaignId));
					$negativecount = $negativecount[0];
					$negativecount = $negativecount['count'];
					
					memcache_set($memcache, 'days_' . $currentCampaignId . '_negative', $negativecount, 5 * 60);
				}
				
				echo "data.addRow([new Date('" . $day['posted_on'] . "'), $dailycount, $positivecount, $negativecount]); \n\t";
			}
		?>
		
    var summarychart = new google.visualization.AreaChart(document.getElementById('overall_twitter_stats'));
    summarychart.draw(data, options);
    
  }

</script>
<?php
}

end_content_for('script');

