<?php

content_for('body');

?>

<h1>Overall Market Reaction</h1>

<div class="row-fluid">
	
	<div class="span12">
		<div id="overall_sentiment_stats" style="width: 95%; height: 500px;"></div>
		<p>
			<a class="btn" href="<?php echo url_for('/campaign/' . $currentCampaignId . '/realtime/sentiment'); ?>">View Twitter Sentiment Dashboard &raquo;</a>
		</p>
	</div>
</div>

<?php
end_content_for();


content_for('script');

?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(sentimentPieChart);
  
  function sentimentPieChart() {
		var options = {
		  title: 'Overall Market Reaction',
		  legend: {
		  	position: 'bottom'
		  }
		};
		
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Mood');
		data.addColumn('number', 'Total');

		<?php
			$neutralcount = $db->run("select count(*) as count from bi_tweetmood where bi_tweetmood.mood = :mood and firehouse_id IN (select firehouse_id from bi_tweets where campaign_id = :cid)", array(":mood" => "neutral", ":cid" => $currentCampaignId));
			$neutralcount = $neutralcount[0];
			$neutralcount = $neutralcount['count'];
			echo "data.addRow(['Neutral ($neutralcount)', $neutralcount]); \n\t";
			
			$positivecount = $db->run("select count(*) as count from bi_tweetmood where bi_tweetmood.mood = :mood and firehouse_id IN (select firehouse_id from bi_tweets where campaign_id = :cid)", array(":mood" => "positive", ":cid" => $currentCampaignId));
			$positivecount = $positivecount[0];
			$positivecount = $positivecount['count'];
			echo "data.addRow(['Positive ($positivecount)', $positivecount]); \n\t";
			
			$negativecount = $db->run("select count(*) as count from bi_tweetmood where bi_tweetmood.mood = :mood and firehouse_id IN (select firehouse_id from bi_tweets where campaign_id = :cid)", array(":mood" => "negative", ":cid" => $currentCampaignId));
			$negativecount = $negativecount[0];
			$negativecount = $negativecount['count'];
			
			echo "data.addRow(['Negative ($negativecount)', $negativecount]); \n\t";
		?>
		
    var sentimentchart = new google.visualization.PieChart(document.getElementById('overall_sentiment_stats'));
    sentimentchart.draw(data, options);
  }

</script>
<?php
end_content_for('script');

