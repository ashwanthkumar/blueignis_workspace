<?php

content_for('body');
?>

<div class="span9">
    <form class="form-horizontal" method="POST" action="<?php echo url_for('/campaign/create/run'); ?>">
		  <fieldset>
				<legend>Create New Campaign</legend>
				<div class="control-group">
					<label class="control-label" for="campaign_name">Campaign Name</label>
					<div class="controls">
						<input type="text" name="campaign_name" class="input-xlarge" id="campaign_name">
						<p class="help-block">&nbsp; </p>
					</div>

					<label class="control-label" for="campaign_tracks">Campaign Tracks</label>
					<div class="controls">
						<input type="text" name="campaign_tracks" class="input-xlarge" id="campaign_tracks">
						<p class="help-block">Use comma (,) to separate multiple keywords </p>
					</div>

					<label class="control-label" for="campaign_service">Social Service</label>
					
					<div class="controls">
						<label class="radio">
							<input type="radio" checked="checked" name="campaign_service" value="Twitter" />
							Twitter
						</label>
						
						<label title="Coming Soon.." class="radio">
							<input type="radio" disabled="disabled" title="Coming Soon..." name="campaign_service" value="Facebook" />
							Facebook 
						</label>
						
						<label title="Coming Soon.." class="radio">
							<input type="radio" disabled="disabled" title="Coming Soon..." name="campaign_service" value="LinkedIn" />
							LinkedIn
						</label>
						<p class="help-block">Use comma (,) to separate multiple keywords </p>
					</div>

					<div class="controls">
						<p>&nbsp; </p>
						<button type="submit" class="btn push-right">Run Campaign &raquo;</button>
					</div>
					
				</div>
		  </fieldset>
    </form>
</div>
<?php
end_content_for('body');

