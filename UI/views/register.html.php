<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>BlueIgnis - Registration </title>
    <meta name="description" content="BlueIgnis - Social Analytics Engine">
    <meta name="author" content="Ashwanth Kumar <ashwanthkumar@googlemail.com>">
 
    <!-- Le styles -->
    <link href="<?php echo url_for('static/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <style type="text/css">
      /* Override some defaults */
      html, body {
        background-color: #eee;
      }
      body {
        padding-top: 40px;
      }

      .container {
        width: 300px;
      }
 
      /* The white background content wrapper */
      .container > .content {
        background-color: #fff;
        padding: 20px;
        margin: 0 -20px;
        -webkit-border-radius: 10px 10px 10px 10px;
           -moz-border-radius: 10px 10px 10px 10px;
                border-radius: 10px 10px 10px 10px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.15);
                box-shadow: 0 1px 2px rgba(0,0,0,.15);
      }
 
      .login-form {
        margin-left: 60px;
      }
      
      .login-form input {
      	margin-left: 5px;
      }
 
      legend {
        margin-right: -50px;
        font-weight: bold;
        color: #404040;
      }
 
    </style>
 
</head>
<body>
	<div class="container">
	  <div class="content">
      <div class="row">
        <center><img style="padding-left: 30px;" src="<?php echo url_for("/static/img/BlueIgnis_login_banner.png"); ?>" /> </center>

        <div class="login-form">
		      <h6> &nbsp; </h6>

				  <div class="alert alert-error">
				  	<p><strong>User Registrations are closed.</strong> </p>
				  </div>

          <form action="<?php echo url_for('/authenticate'); ?>" method="POST">
              <fieldset>
                  <?php
                  
                  if(isset($_SESSION['invalid_login'])) {
                  ?>
                  <div class="alert alert-error">
                  <a class="close">&times;</a>

                  <strong>Unable to login</strong> <?php echo $_SESSION['invalid_login']; ?>
                  </div>
                  <?php
                          unset($_SESSION['invalid_login']);  // Since this is a flash type message dismiss on refresh
                      }
                  ?>
                  <!-- Say that Registrations are currently closed -->
                  
                  <div class="clearfix">
                      <input type="text" name="name" placeholder="Full Name">
                  </div>
                  <div class="clearfix">
                      <input type="text" name="username" placeholder="EMail ID">
                  </div>
                  <div class="clearfix">
                      <input type="password" name="password" placeholder="Password">
                  </div>
                  <div class="clearfix">
                      <input type="password" name="rpassword" placeholder="Retype Password">
                  </div>
                  <div class="clearfix">
                      <input type="text" name="company" placeholder="Organization">
                  </div>

							    <a class="btn btn-default" href="<?php echo url_for("/login"); ?>"><i class="icon-fire"></i> Login </a>
                  <button class="btn btn-primary disabled" disabled="disabled" type="submit"><i class="icon-user icon-white "></i> Register</button>
							    <a class="btn btn-default" href="#intro"><i class="icon-facetime-video"></i> Lost? </a>
              </fieldset>
          </form>
          <hr />
          <p>&copy; <a href="http://ashwanthkumar.in/">Ashwanth Kumar</a> Productions, 2012. </p>
        </div>
      </div>
	  </div>
	</div> <!-- /container -->
</body>
</html>
