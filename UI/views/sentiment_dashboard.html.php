<?php
	
	content_for('body');
?>

<div class="span6">
	<div class="stream-group">
		<div class="stream">
		<div class="stream-inner">

		 <div class="stream-header">
		   <h3>Positive Tweets</h3>
		 </div><!-- /stream-header -->

		 <div class="positive-stream-body">

<?php
	$tweets = $db->run("select bi_tweets.Id as id, bi_firehouse.text as text, bi_firehouse.user_id as user_id, bi_tusers.profileImage as profile_image_url, bi_tusers.screenName as screen_name, bi_tweets.campaign_id as campaign_id from bi_tweets, bi_firehouse, bi_tusers, bi_tweetmood where bi_tweets.firehouse_id = bi_firehouse.id and bi_tweetmood.firehouse_id = bi_firehouse.id and bi_tusers.id = bi_firehouse.user_id and bi_tweetmood.mood = :mood and bi_tweets.campaign_id = :cid order by bi_firehouse.createdAt desc limit 0,10", array(":mood" => "positive", ":cid" => $currentCampaignId));
	foreach($tweets as $tweet) {
?>
			 <div class="positive-stream-item  stream-item">
			   <div class="stream-item-inner">
			   <div class="stream-item-icon">
				 <img src="<?php echo $tweet['profile_image_url']; ?>">
			   </div>
			   <div class="stream-item-title">
				  <h3>@<?php echo $tweet['screen_name']; ?></h3>
			   </div>

			   <div class="stream-item-content">
				 <p><?php 
				 	 // Convert String links to hyperlinks
					 $status_text = preg_replace( '/(https?:\/\/\S+)/', '<a href="\1">\1</a>',$tweet['text']);
					 // Identify the users on the feed and add their Twitter profile links
					 $status_text = preg_replace('/@(\S+)/', '@<a href="http://twitter.com/\1">\1</a>', $status_text);
					 echo $status_text;
				 ?></p>
			   </div>

			   </div>
			 </div>
<?php
	}	// End of foreach Positive Tweet Stream
?>
		 </div><!-- /positive-stream-body -->

		</div><!--/stream-inner -->
		</div><!-- /stream -->	
	</div>
</div>

<div class="span6">
	<div class="stream-group">
		<div class="stream">
		<div class="stream-inner">

		 <div class="stream-header">
		   <h3>Negative Tweets</h3>
		 </div><!-- /stream-header -->

		 <div class="negative-stream-body">
<?php
	$tweets = $db->run("select bi_tweets.Id as id, bi_firehouse.text as text, bi_firehouse.user_id as user_id, bi_tusers.profileImage as profile_image_url, bi_tusers.screenName as screen_name, bi_tweets.campaign_id as campaign_id from bi_tweets, bi_firehouse, bi_tusers, bi_tweetmood where bi_tweets.firehouse_id = bi_firehouse.id and bi_tweetmood.firehouse_id = bi_firehouse.id and bi_tusers.id = bi_firehouse.user_id and bi_tweetmood.mood = :mood and bi_tweets.campaign_id = :cid order by bi_firehouse.createdAt desc limit 0,10", array(":mood" => "negative", ":cid" => $currentCampaignId));
	foreach($tweets as $tweet) {
?>
			 <div class="negative-stream-item stream-item">
			   <div class="stream-item-inner">
			   <div class="stream-item-icon">
				 <img src="<?php echo $tweet['profile_image_url']; ?>">
			   </div>
			   <div class="stream-item-title">
				  <h3>@<?php echo $tweet['screen_name']; ?></h3>
			   </div>

			   <div class="stream-item-content">
				 <p><?php 
					 $status_text = preg_replace( '/(https?:\/\/\S+)/', '<a href="\1">\1</a>',$tweet['text']);
					 $status_text = preg_replace('/@(\S+)/', '@<a href="http://twitter.com/\1">\1</a>', $status_text);
					 echo $status_text;
				 ?></p>
			   </div>

			   </div>
			 </div>
<?php
	}	// End of foreach Positive Tweet Stream
?>

		 </div><!-- /negative-stream-body -->
		</div><!--/stream-inner -->
		</div><!-- /stream -->	
	</div>
</div>

<?php
	end_content_for('body');

