<?php

content_for('body');

?>

<h1>Source of Tweets</h1>

<div class="row-fluid">
	
	<div class="span12">
		<div id="overall_source_stats" style="width: 95%; height: 500px;"></div>
		<!--<p>
			<a class="btn" href="<?php echo url_for('/campaign/' . $currentCampaignId . '/realtime/sentiment'); ?>">View Twitter Sentiment Dashboard &raquo;</a>
		</p>-->
	</div>
</div>

<?php
end_content_for();

content_for('script');
?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(usageSourcePieChart);
  
  function usageSourcePieChart() {
		var options = {
		  title: 'Top 10 Tweet Sources',
		  legend: {
		  	position: 'left'
		  }
		};
		
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Source');
		data.addColumn('number', 'Total');

		<?php
			$sources = $db->run("select count(*) as count, source from bi_firehouse where id in (select firehouse_id from bi_tweets where campaign_id = :cid) group by source order by count desc limit 10", array(":cid" => $currentCampaignId));
			
			foreach($sources as $s) 
				echo "data.addRow(['" .preg_replace("/<a(.+)>(.+)<\/a>/", "\\2", $s['source']) . "'," . $s['count'] . "]); \n\t";
		?>
		
    var sentimentchart = new google.visualization.PieChart(document.getElementById('overall_source_stats'));
    sentimentchart.draw(data, options);
  }

</script>
<?php
end_content_for('script');

