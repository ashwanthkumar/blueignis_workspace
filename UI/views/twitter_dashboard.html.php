<?php
	
	content_for('body');
?>
	<div class="stream-group">
		<div class="stream">

		<div class="stream-inner">

		 <div class="stream-header">
		   <h3>Realtime Twitter Dashboard</h3>
		 </div><!-- /stream-header -->
		 <div class="stream-body">
<?php
	$tweets = $db->run("select bi_tweets.Id as id, bi_firehouse.text as text, bi_firehouse.user_id as user_id, bi_tusers.profileImage as profile_image_url, bi_tusers.screenName as screen_name, bi_tweets.campaign_id as campaign_id from bi_tweets, bi_firehouse, bi_tusers where bi_tweets.firehouse_id = bi_firehouse.id and bi_tusers.id = bi_firehouse.user_id and bi_tweets.campaign_id = :cid order by bi_firehouse.createdAt desc limit 0,10", array(":cid" => $currentCampaignId));
	foreach($tweets as $tweet) {
?>
			 <div class="stream-item">
			   <div class="stream-item-inner">
			   <div class="stream-item-icon">
				 <img src="<?php echo $tweet['profile_image_url']; ?>">
			   </div>
			   <div class="stream-item-title">
				  <h3>@<?php echo $tweet['screen_name']; ?></h3>
			   </div>

			   <div class="stream-item-content">
				 <p><?php 
				 	 // Convert String links to hyperlinks
					 $status_text = preg_replace( '/(https?:\/\/\S+)/', '<a href="\1">\1</a>',$tweet['text']);
					 // Identify the users on the feed and add their Twitter profile links
					 $status_text = preg_replace('/@(\S+)/', '@<a href="http://twitter.com/\1">\1</a>', $status_text);
					 echo $status_text;
				 ?></p>
			   </div>

			   </div>
			 </div>
<?php
	}	// End of foreach Positive Tweet Stream

?>
		 </div><!-- /stream-body -->
		</div><!-- /stream-inner -->
		</div><!-- /stream -->	
	</div>
<!-- Stream Based Stuff ends -->		

<?php
	end_content_for('body');

